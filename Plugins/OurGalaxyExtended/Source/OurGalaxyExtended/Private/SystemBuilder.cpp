// Gradess & Novaturion Company. All rights reserved.


#include "SystemBuilder.h"

DEFINE_LOG_CATEGORY(LogSystemBuilder);

// Sets default values
ASystemBuilder::ASystemBuilder()
{

}

void ASystemBuilder::SetActorSceneName_Implementation(AActor* TargetActor, const FString& Label, const bool bMarkDirty)
{
#if WITH_EDITOR
    TargetActor->SetActorLabel(Label, bMarkDirty);
#else
    UE_LOG(
        LogSystemBuilder,
        Warning,
        TEXT("%s::%s; Line: %d; %s"),
        *FString(__FILE__),
        *FString(__FUNCTION__),
        __LINE__,
        *FString("SetActorLabel is only available in editor!")
    );
#endif
}
